# -*- coding: utf-8 -*-
"""
Created on Wed May 20 21:32:40 2020

@author: Dell-pc
"""

import matplotlib.pyplot as plt
import tensorflow as tf


x = tf.zeros([10,10])
x += 2 # Does not mutate the original value of x
print(x)

v = tf.Variable(1.0)
assert v.numpy() == 1.0

v.assign(3.0)
assert v.numpy() == 3.0

v.assign(tf.square(v))
assert v.numpy() == 9.0

class Model(object):
    def __init__(self):
        self.W = tf.Variable(5.0)
        self.b = tf.Variable(0.0)
    
    def __call__(self,x):
        return self.W*x + self.b
        pass
    
model = Model()
assert model(3.0).numpy() == 15.0

def loss(target_y, pred_y):
    return tf.reduce_mean(tf.square(target_y-pred_y))

TRUE_W = 3.0
TRUE_b = 2.0
NUM_EXAMPLES = 1000

inputs = tf.random.normal(shape=[NUM_EXAMPLES])
noise = tf.random.normal(shape=[NUM_EXAMPLES])
outputs = inputs*TRUE_W + TRUE_b + noise

plt.scatter(inputs,outputs,color='b')
plt.scatter(inputs,model(inputs),c='r')
plt.show()

print("Current Loss: %1.6f" % loss(model(inputs), outputs).numpy())

def train(model,inputs,outputs, learning_rate):
    with tf.GradientTape() as t:
        current_loss = loss(outputs,model(inputs))
    
    dW,db = t.gradient(current_loss, [model.W,model.b])
    model.W.assign_sub(learning_rate*dW)
    model.b.assign_sub(learning_rate*db)
    
model = Model()

Ws,bs = [], []
epochs = range(10)

for epoch in epochs:
    Ws.append(model.W.numpy())
    bs.append(model.b.numpy())
    current_loss = loss(outputs, model(inputs))
    
    train(model,inputs,outputs,learning_rate=0.1)
    print("Epoch: ",epoch," W: ",Ws[-1]," b: ",bs[-1]," Loss: ",current_loss.numpy())
    
plt.plot(epochs,Ws, 'r',
         epochs,bs, 'b')
plt.plot([TRUE_W]*len(epochs),'r--',
         [TRUE_b]*len(epochs),'b--')
plt.legend(['W','b','TRUE W','TRUE b'])
plt.show()